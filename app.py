from flask import Flask, request, render_template
import json

app = Flask(__name__)

tasks = [
    {
        "id": 2200519,
        "nome":"Amalia Emilia da Rocha Pitta"
    },
    {
        "id": 2202122,
        "nome":"Beatriz Borges Cantero"
    },
    {
        "id": 2200770,
        "nome":"Guilherme Midea Paoliello Castilho"
    },
    {
        "id": 2200545,
        "nome":"Gustavo Holanda Soares Santana"
    }
]

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/consultar/<int:id>', methods=["GET"])
def consultar(id):
    try:
        for task in tasks:
            if id == task["id"]:
                return task
    except:
        return {"error": "Usuário não encontrado"} 
       
@app.route('/usuarios', methods=["GET"])
def verificar():        
    return{"usuarios": tasks}

@app.route('/cadastrar', methods=["POST"])
def cadastrar():
    input_json = request.get_json()
    tasks.append(input_json)
    return {"Resultado":"JSON adicionado com sucesso. Lista de dados atualizada"}

@app.route('/deletar/<int:id>', methods=["DELETE"])
def deletar(id):
    for task in tasks:
        if id == task["id"]:
            tasks.remove(task)
    return "Excluido usuário de Id: " + str(id)
    
@app.route('/atualizar/<int:id>', methods=["PUT"])
def atualizar(id):
    input_json = request.get_json()
    print(input_json)
    for task in tasks:
        if id == task["id"]:
            task["nome"] = input_json["nome"]
            return "Usuário de id:" + str(id) + " foi atualizado!"
        

if __name__ == '__main__':
    app.run(debug=True)